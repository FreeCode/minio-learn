package com.tianhao.luo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName: Configuration
 * @Description: 关联yml中的配置文件
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/7  13:32
 */
@Component
@ConfigurationProperties(prefix = "com.tianhao.luo")
public class Configuration {
    private Minio minio;
    private Nginx nginx;
    private ServletResponse servletResponse;
    private ServletRequest servletRequest;

    public ServletRequest getServletRequest() {
        return servletRequest;
    }

    public void setServletRequest(ServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    public ServletResponse getServletResponse() {
        return servletResponse;
    }

    public void setServletResponse(ServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    public Minio getMinio() {
        return minio;
    }

    public void setMinio(Minio minio) {
        this.minio = minio;
    }

    public Nginx getNginx() {
        return nginx;
    }

    public void setNginx(Nginx nginx) {
        this.nginx = nginx;
    }

    public static class Minio{
        private String bucket = "minio-learn";
        private String endPoint = "http://192.168.15.78:9000";
        private String accessKey = "root";
        private String secretKey = "password";
        private String algorithm = "md5";

        public String getBucket() {
            return bucket;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        public String getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getAccessKey() {
            return accessKey;
        }

        public void setAccessKey(String accessKey) {
            this.accessKey = accessKey;
        }

        public String getSecretKey() {
            return secretKey;
        }

        public void setSecretKey(String secretKey) {
            this.secretKey = secretKey;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public void setAlgorithm(String algorithm) {
            this.algorithm = algorithm;
        }
    }

    public static class Nginx{
        private String localtion = "http://192.168.15.78:8080";

        public String getLocaltion() {
            return localtion;
        }

        public void setLocaltion(String localtion) {
            this.localtion = localtion;
        }
    }

    public static class ServletResponse{
        private Header header;
        private File file;

        public Header getHeader() {
            return header;
        }

        public void setHeader(Header header) {
            this.header = header;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public static class Header{
            private String length = "Content-Length";
            private String disposition="content-disposition";

            public String getLength() {
                return length;
            }

            public void setLength(String length) {
                this.length = length;
            }

            public String getDisposition() {
                return disposition;
            }

            public void setDisposition(String disposition) {
                this.disposition = disposition;
            }
        }

        public static class File{
            private String charsetName = "ISO8859-1";

            public String getCharsetName() {
                return charsetName;
            }

            public void setCharsetName(String charsetName) {
                this.charsetName = charsetName;
            }
        }
    }

    public static class  ServletRequest{
        private File file;

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public static class File{
            private String spilt;

            public String getSpilt() {
                return spilt;
            }

            public void setSpilt(String spilt) {
                this.spilt = spilt;
            }
        }
    }
}
