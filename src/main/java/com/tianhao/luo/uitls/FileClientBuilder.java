package com.tianhao.luo.uitls;

import com.tianhao.luo.config.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: FileClientBuilder
 * @Description: 文件客户端构造器，用来完成文件的构造
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/7  14:45
 */
@Component
public class FileClientBuilder {
    @Autowired
    private Configuration configuration;

    private FileClient fileClient;

    public FileClient getFileClient() {
        return fileClient;
    }

    public void setFileClient(FileClient fileClient) {
        this.fileClient = fileClient;
    }

    public FileClientBuilder builder(){
        FileClientBuilder fileClientBuilder = new FileClientBuilder();
        fileClientBuilder.setFileClient(new FileClient(configuration));
        return fileClientBuilder;
    }

    public FileClient init(){
        fileClient.init();
        return fileClient;
    }
}
