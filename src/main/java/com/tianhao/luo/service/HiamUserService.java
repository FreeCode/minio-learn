package com.tianhao.luo.service;

import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.uitls.CommonException;

/**
 * @ClassName: HiamUserService
 * @Description: 用户服务
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:34
 */
public interface HiamUserService {

    /**
     * 插入一个用户
     * @param hiamUser 用户基本信息
     * @return 用户基本信息
     * @throws CommonException 业务异常
     */
    HiamUser createUser(HiamUser hiamUser) throws CommonException;
}
