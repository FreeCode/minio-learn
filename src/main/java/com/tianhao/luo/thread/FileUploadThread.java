package com.tianhao.luo.thread;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.domain.mapper.HfleFileMapper;
import com.tianhao.luo.domain.repository.HfleFileRepository;
import com.tianhao.luo.uitls.FileClientBuilder;
import com.tianhao.luo.uitls.FileOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @ClassName: FileUploadThread
 * @Description: 上传文件线程
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  8:34
 */
public class FileUploadThread implements Callable<List<HfleFile>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadThread.class);
    private FileClientBuilder fileClientBuilder;
    private List<MultipartFile> files;
    private HiamUser hiamUser;
    private HfleFileMapper hfleFileMapper;
    private HfleFileRepository hfleFileRepository;
    private MultipartFile file;

    public FileUploadThread(FileClientBuilder fileClientBuilder, List<MultipartFile> files, HiamUser hiamUser, HfleFileMapper hfleFileMapper, HfleFileRepository hfleFileRepository) {
        this.fileClientBuilder = fileClientBuilder;
        this.files = files;
        this.hiamUser = hiamUser;
        this.hfleFileMapper = hfleFileMapper;
        this.hfleFileRepository = hfleFileRepository;
    }

    public FileUploadThread(FileClientBuilder fileClientBuilder, MultipartFile file, HiamUser hiamUser, HfleFileMapper hfleFileMapper, HfleFileRepository hfleFileRepository) {
        this.fileClientBuilder = fileClientBuilder;
        this.hiamUser = hiamUser;
        this.hfleFileMapper = hfleFileMapper;
        this.hfleFileRepository = hfleFileRepository;
        this.file = file;
    }

    private HfleFile save(HiamUser hiamUser, MultipartFile file) {
        LOGGER.info("当前文件上传线程：{}",Thread.currentThread());
        //获取文件上传客户端
        FileOperator<HfleFile, HiamUser> fileClient = fileClientBuilder.builder().init();
        //构造md5查询条件
        HfleFile tempFile = new HfleFile();
        tempFile.setMd5(fileClient.getFileMd5(file));
        //通过md5，查找系统中是否存在了这个文件
        HfleFile existFile = hfleFileMapper.selectLimitOne(tempFile);
        //构造当前用户查找当前文件的条件
        tempFile.setTenantId(hiamUser.getTenantId());
        HfleFile userExistFile = hfleFileMapper.selectLimitOne(tempFile);
        //用户没有上传过文件，需要上传
        if (ObjectUtils.isEmpty(userExistFile)) {
            //系统中没有这个文件
            if (ObjectUtils.isEmpty(existFile)) {
                LOGGER.info("tenantId:{}的用户上传了新的文件:{}进入系统！", hiamUser.getTenantId(), file.getOriginalFilename());
                return hfleFileRepository.save(fileClient.uploadFile(hiamUser, file));
            } else {//系统中有这个文件
                //更新所属用户
                LOGGER.info("tenantId:{}的用户通过别人已上传的文件:{}实现了急速秒传！", hiamUser.getTenantId(), existFile.getFileName());
                existFile.setTenantId(hiamUser.getTenantId());
                existFile.setFileName(file.getOriginalFilename());
                return hfleFileRepository.save(existFile);
            }
        } else {//用户上传过这个文件，直接返回文件信息
            LOGGER.info("用户已经上传过该文件:{}了！", file.getOriginalFilename());
            return userExistFile;
        }
    }

    @Override
    public List<HfleFile> call() throws Exception {
        List<HfleFile> hfleFiles = new ArrayList<>();
        //单文件上传
        if (CollectionUtils.isEmpty(this.files)){
            hfleFiles.add(this.save(this.hiamUser,this.file));
        }else { //多文件上传
            for (MultipartFile file : files) {
                hfleFiles.add(this.save(hiamUser, file));
            }
        }
        return hfleFiles;
    }
}
