package com.tianhao.luo.thread;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName: MyInstanceBuilder
 * @Description: 用来生成复杂对象，方法函数
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  13:50
 */
public interface MyInstanceBuilder<T,k> {

    /**
     * 构造复杂对象
     * @param user 业务对象
     * @param file 文件
     * @return 自己的文件类型
     */
    T build(k user, MultipartFile file);

    /**
     * 构造复杂对象
     * @param user 业务对象
     * @param file 文件
     * @return 自己的文件类型
     */
    T build(k user, List<MultipartFile> file);
}
