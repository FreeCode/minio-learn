package com.tianhao.luo.domain.repository;

import com.tianhao.luo.domain.entity.HiamUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName: HiamUserRepository
 * @Description: 利用jpa实现基本的crud
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:18
 */
public interface HiamUserRepository extends JpaRepository<HiamUser,Long> {
}
