package com.tianhao.luo.annotation;

import java.lang.annotation.*;

/**
 * @ClassName: UserFileRecode
 * @Description: 文件接口用户的操作记录
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  8:37
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
@Documented
public @interface UserFileRecodeAnnotation {
    boolean value() default true;
}
