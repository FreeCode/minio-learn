package com.tianhao.luo.controller;

import com.tianhao.luo.annotation.UserFileRecodeAnnotation;
import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.service.HfleFileService;
import com.tianhao.luo.service.UserFileService;
import com.tianhao.luo.uitls.ResponseData;
import com.tianhao.luo.uitls.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName: HfleFileController
 * @Description: 提供文件服务接口，包含上传文件，下载文件和删除文件。
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  22:08
 */
@RestController
@RequestMapping("/file")
public class HfleFileController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HfleFileController.class);
    @Autowired
    private HfleFileService hfleFileService;
    @Autowired
    private UserFileService userFileService;

    /**
     * 上传文件
     * @param hiamUser 用户基本信息
     * @param file 文件
     * @return 数据库中文件的基本信息
     */
    @PostMapping
    public ResponseData<HfleFile> saveFile(HiamUser hiamUser,MultipartFile file){
        return Result.success(hfleFileService.save(hiamUser, file));
    }

    /**
     * 浏览器文件下载
     * @param hfleFile 文件对象
     * @param httpServletResponse 响应
     */
    @GetMapping("/download")
    public void downLoadFile(HfleFile hfleFile, HttpServletResponse httpServletResponse){
        hfleFileService.downLoadFile(hfleFile,httpServletResponse);
    }

    /**
     * 上传文件
     * @param hiamUser 用户基本信息
     * @param files 文件
     * @return 数据库中文件的基本信息
     */
    @PostMapping("/files")
    public ResponseData<List<HfleFile>> batchSaveFile(HiamUser hiamUser, List<MultipartFile> files) throws Exception{
        return Result.success(hfleFileService.batchSaveFile(hiamUser, files));
    }

    /**
     * 分页查询用户文件
     * @param hiamUser 查询的用户
     * @param page 页码
     * @param size 一页多少记录
     * @return 文件的分页
     */
    @GetMapping("/user")
    public ResponseData<Page<HfleFile>> queryUsersFile(HiamUser hiamUser, int page, int size){
        return Result.success(userFileService.queryUsersFile(hiamUser, PageRequest.of(page, size)));
    }

    /**
     * 通过文件类型，分页查询用户文件
     * @param hiamUser 查询的用户
     * @param page 页码
     * @param size 一页多少记录
     * @return 文件的分页
     */
    @GetMapping("/user/type")
    public ResponseData<Page<HfleFile>> queryUsersFileByType(HiamUser hiamUser,String type,int page, int size){
        return Result.success(userFileService.queryUsersFileByType(hiamUser,type,PageRequest.of(page, size)));
    }

    /**
     * 查询用户的某一个文件
     * @param hiamUser 用户
     * @param uuid 文件uuid标识
     * @return 文件信息
     */
    @GetMapping("/user/{uuid}")
    public ResponseData<HfleFile> queryFileByUserAndUuid(HiamUser hiamUser,@PathVariable String uuid){
        return Result.success(userFileService.queryFileByUserAndUuid(hiamUser, uuid));
    }

    /**
     * 查询用户的某一个文件
     * @param hiamUser 用户
     * @param attachmentUuid 文件uuid标识
     */
    @GetMapping("/user/download/{attachmentUuid}")
    @UserFileRecodeAnnotation()
    public void downloadFileByUserAndUuid(HiamUser hiamUser,@PathVariable String attachmentUuid,HttpServletResponse httpServletResponse) {
        HfleFile hfleFile = new HfleFile();
        hfleFile.setTenantId(hiamUser.getTenantId());
        hfleFile.setAttachmentUuid(attachmentUuid);
        if (!ObjectUtils.isEmpty(userFileService.queryFileByUserAndUuid(hiamUser, attachmentUuid))){
            hfleFileService.downLoadFile(hfleFile,httpServletResponse);
        }else {
            LOGGER.info("用户没有该文件！");
        }
    }
}
