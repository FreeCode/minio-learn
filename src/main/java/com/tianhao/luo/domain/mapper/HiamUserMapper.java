package com.tianhao.luo.domain.mapper;

import com.tianhao.luo.domain.entity.HiamUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName: HiamUserMapper
 * @Description: 用户查询mapper
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  20:52
 */
@Mapper
public interface HiamUserMapper {
    HiamUser selectAnUser(@Param("hiamUser") HiamUser hiamUser);
}
