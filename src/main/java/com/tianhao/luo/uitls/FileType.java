package com.tianhao.luo.uitls;

/**
 * @ClassName: PictureType
 * @Description: 文件格式对应minio中格式
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/7  9:46
 */
public enum FileType {
    /**
     * 图片格式类型
     */
    IMAGE_PNG("png","image/png"),
    IMAGE_JPEG("jpeg","image/jpeg"),
    IMAGE_JPG("jpg","image/jpg"),
    IMAGE_BMP("bmp","image/bmp"),
    /**
     * 文件格式类型
     */
    APPLICATION_PDF("pdf","application/pdf"),
    VIDEO_AVI("avi","video/avi"),
    VIDEO_MP4("mp4","video/mp4"),
    ;

    private String type;
    private String minioType;

    FileType(String type, String minioType) {
        this.type = type;
        this.minioType = minioType;
    }

    public String getType() {
        return type;
    }

    public String getMinioType() {
        return minioType;
    }
}
