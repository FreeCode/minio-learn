package com.tianhao.luo.service.impl;

import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.domain.repository.HiamUserRepository;
import com.tianhao.luo.service.HiamUserService;
import com.tianhao.luo.uitls.CommonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName: HiamUserServiceImpl
 * @Description: 用户服务实现
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:35
 */
@Service("HiamUserServiceImpl")
public class HiamUserServiceImpl implements HiamUserService {
    @Autowired
    private HiamUserRepository userRepository;


    @Override
    public HiamUser createUser(HiamUser user) throws CommonException {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new CommonException("插入用户失败"+e.getMessage(), "user_insert_error");
        }
    }
}
