package com.tianhao.luo.thread;

import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.domain.mapper.HfleFileMapper;
import com.tianhao.luo.domain.repository.HfleFileRepository;
import com.tianhao.luo.uitls.FileClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName: FileUploadThreadBuilder
 * @Description: 用来构造文件上传的线程
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  13:48
 */
@Component
public class FileUploadThreadBuilder implements MyInstanceBuilder<FileUploadThread,HiamUser> {

    @Autowired
    private HfleFileRepository hfleFileRepository;
    @Autowired
    private HfleFileMapper hfleFileMapper;
    @Autowired
    private FileClientBuilder fileClientBuilder;



    @Override
    public FileUploadThread build(HiamUser user, MultipartFile file) {
        return new FileUploadThread(this.fileClientBuilder, file, user, this.hfleFileMapper, this.hfleFileRepository);
    }

    @Override
    public FileUploadThread build(HiamUser user, List<MultipartFile> file) {
        return new FileUploadThread(this.fileClientBuilder, file, user, this.hfleFileMapper, this.hfleFileRepository);
    }
}
