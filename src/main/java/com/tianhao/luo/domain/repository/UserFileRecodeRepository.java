package com.tianhao.luo.domain.repository;

import com.tianhao.luo.domain.entity.UserFileRecode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName: UserFileRecodeRepository
 * @Description: 用户文件记录查询
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  10:50
 */
public interface UserFileRecodeRepository extends JpaRepository<UserFileRecode,Long> {
}
