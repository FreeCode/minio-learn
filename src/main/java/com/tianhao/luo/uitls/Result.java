package com.tianhao.luo.uitls;

/**
 * @ClassName: Rsult
 * @Description: 响应信息格式化工具类
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:26
 */
public final class Result {

    public static <T> ResponseData<T> success(T obj) {
        return new ResponseData<T>(obj,"success",200);
    }

    public static <T> ResponseData<T> fail(T obj){
        return new ResponseData<>(obj,"failed",400);
    }
}
