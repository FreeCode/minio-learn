package com.tianhao.luo.uitls;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName: FileOperator
 * @Description: 提供文件上传的标准和获取md5的方法
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/7  14:47
 */
public interface FileOperator<T,E> {
    /**
     * 上传文件
     * @param e 用户或者其他需要保存的基本信息
     * @param file 文件
     * @return 数据库中文件表的类型
     */
    T uploadFile(E e, MultipartFile file);

    /**
     * 获取文件的md5值
     * @param file 文件
     * @return md5字符串
     */
    String getFileMd5(MultipartFile file);

    /**
     * 浏览器下载文件
     * @param t 文件参数
     * @param httpServletResponse 请求响应
     */
    void downLoadFile(T t, HttpServletResponse httpServletResponse);
}
