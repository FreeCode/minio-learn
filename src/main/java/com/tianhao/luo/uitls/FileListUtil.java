package com.tianhao.luo.uitls;

import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: FileListUtil
 * @Description: 用来拆分文件的
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  9:29
 */
public class FileListUtil {

    public static <T> List<List<T>> spilt(int count, List<T> dataList){
        int size = dataList.size();
        Assert.isTrue(size>count,"当前list长度不用划分");
        ArrayList<List<T>> spiltList = new ArrayList<>();
        int integerItem = size / count;
        int remainderItem = size % count;
        //如果能被整除，则list的长度就是size/count；否则是size/count +1
        for (int i = 0; remainderItem == 0?i < integerItem :i< integerItem +1; i++) {
            ArrayList<T> listOne = new ArrayList<>(count);
            int integerItemIndex = i * count;
            int integerItemNextIndex = (i + 1) * count;
            //如果dataList不能被count整除，则当前循环的限制条件最大值为传入datalist的长度（也就是integerItemNextIndex），否则是当前的整数部分+余数部分
            for (int j = integerItemIndex; integerItemNextIndex <=size ? j < integerItemNextIndex :j< integerItemIndex + remainderItem; j++) {
                listOne.add(dataList.get(j));
            }
            spiltList.add(listOne);
        }
        return spiltList;
    }
}
