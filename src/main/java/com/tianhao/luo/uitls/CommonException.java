package com.tianhao.luo.uitls;

/**
 * @ClassName: CommonException
 * @Description: 提供业务异常
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  18:03
 */
public class CommonException extends RuntimeException {

    private String msg;
    private String code;

    public CommonException(String msg, String code) {
        this.msg = msg;
        this.code = code;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CommonException{");
        sb.append("msg='").append(msg).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
