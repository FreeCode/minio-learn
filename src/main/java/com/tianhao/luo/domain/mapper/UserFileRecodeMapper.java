package com.tianhao.luo.domain.mapper;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.domain.entity.UserFileRecode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @ClassName: UserFileRecodeMapper
 * @Description: 用户操作文件记录
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  9:16
 */
@Mapper
public interface UserFileRecodeMapper {

    /**
     * 插入用户下载文件记录
     * @param hiamUser 用户
     * @param uuid 文件标识
     * @param date 下载时间
     */
    void insertRecode(@Param("hiamUser") HiamUser hiamUser,String uuid, Date date);


    /**
     * 最高下载记录
     * @return 下载记录
     */
    List<UserFileRecode> listTopTenUserFileRecode(LocalDateTime start, LocalDateTime end);

    /**
     * 文件list
     * @param fileRecodes 文件记录
     * @return 文件
     */
    List<HfleFile> listTopTenHfleFile(List<UserFileRecode> fileRecodes);
}
