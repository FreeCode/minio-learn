package com.tianhao.luo.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 
 *
 * @author tianhao.luo@hand-china.com 2020-08-06 17:16:11
 */

@JsonInclude(value = JsonInclude.Include.NON_NULL)//为空的字段不显示
@Entity//声明这是一个与表映射的实体
@Table(name = "hfle_file")//表名
public class HfleFile extends AuditDomain {

    public static final String FIELD_FILE_ID = "fileId";
    public static final String FIELD_ATTACHMENT_UUID = "attachmentUuid";
    public static final String FIELD_DIRECTORY = "directory";
    public static final String FIELD_FILE_URL = "fileUrl";
    public static final String FIELD_FILE_TYPE = "fileType";
    public static final String FIELD_FILE_NAME = "fileName";
    public static final String FIELD_FILE_SIZE = "fileSize";
    public static final String FIELD_BUCKET_NAME = "bucketName";
    public static final String FIELD_MD5 = "md5";
    public static final String FIELD_TENANT_ID = "tenantId";

    //
    // 业务方法(按public protected private顺序排列)
    // ------------------------------------------------------------------------------

    //
    // 数据库字段
    // ------------------------------------------------------------------------------


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fileId;
    @NotBlank
    private String attachmentUuid;
    private String directory;
    @NotBlank
    private String fileUrl;
    private String fileType;
    @NotBlank
    private String fileName;
    private Long fileSize;
    @NotBlank
    private String bucketName;
    private String md5;
    @NotNull
    private Long tenantId;

	//
    // 非数据库字段
    // ------------------------------------------------------------------------------

    //
    // getter/setter
    // ------------------------------------------------------------------------------

    /**
     * @return 
     */
	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}
    /**
     * @return 附件集UUID
     */
	public String getAttachmentUuid() {
		return attachmentUuid;
	}

	public void setAttachmentUuid(String attachmentUuid) {
		this.attachmentUuid = attachmentUuid;
	}
    /**
     * @return 上传目录
     */
	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}
    /**
     * @return 文件地址
     */
	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
    /**
     * @return 文件类型
     */
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
    /**
     * @return 文件名称
     */
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
    /**
     * @return 文件大小
     */
	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
    /**
     * @return 文件目录
     */
	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}
    /**
     * @return 文件MD5
     */
	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}
    /**
     * @return 租户ID
     */
	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

}
