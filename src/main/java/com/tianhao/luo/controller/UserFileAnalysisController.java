package com.tianhao.luo.controller;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.service.UserFileAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: UserFileAnalysisController
 * @Description: 分析出下载排行量
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  10:43
 */
@RestController
@RequestMapping("/user-file")
public class UserFileAnalysisController {

    @Autowired
    private UserFileAnalysisService userFileAnalysisService;

    /**
     * 一天最高下载榜单
     * @return 文件记录
     */
    @GetMapping("/day")
    public List<HfleFile> listTopTenFromCurrentDay(){
        return userFileAnalysisService.listTopTenFromCurrentDay();
    }

    /**
     * 一周最高下载榜单
     * @return 文件记录
     */
    @GetMapping("/week")
    public List<HfleFile> listTopTenFromCurrentWeek(){
        return userFileAnalysisService.listTopTenFromCurrentWeek();
    }
    /**
     * 一月最高下载榜单
     * @return 文件记录
     */
    @GetMapping("/month")
    public List<HfleFile> listTopTenFromCurrentMonth(){
        return userFileAnalysisService.listTopTenFromCurrentMonth();
    }
}
