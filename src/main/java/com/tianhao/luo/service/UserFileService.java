package com.tianhao.luo.service;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;

/**
 * @ClassName: UserFileService
 * @Description: 用户文件服务
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  20:32
 */
public interface UserFileService {
    /**
     * 分页查询用户文件
     * @param hiamUser 查询的用户
     * @param pageRequest 分页请求
     * @return 用户文件
     */
    Page<HfleFile> queryUsersFile(HiamUser hiamUser, PageRequest pageRequest);

    /**
     * 通过文件类型，分页查询用户文件
     * @param hiamUser 查询的用户
     * @param type 文件类型
     * @param pageRequest 文件分页
     * @return 用户文件
     */
    Page<HfleFile> queryUsersFileByType(HiamUser hiamUser, String type,PageRequest pageRequest);

    /**
     * 查询用户的某一个文件
     * @param hiamUser 用户
     * @param uuid 文件uuid标识
     * @return 文件信息
     */
    HfleFile queryFileByUserAndUuid(HiamUser hiamUser, String uuid);

    /**
     * 插入用户下载文件记录
     * @param hiamUser 用户
     * @param uuid 文件标识
     * @param date 下载时间
     */
    void insertRecode( HiamUser hiamUser, String uuid, Date date);
}
