package com.tianhao.luo.domain.mapper;

import com.tianhao.luo.domain.entity.HfleFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName: HfleFileMapper
 * @Description: 文件接口
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/7  14:21
 */
@Mapper
public interface HfleFileMapper {

    /**
     * 只能查出一条记录
     * @param hfleFile 条件
     * @return 文件实体
     */
    HfleFile selectLimitOne(@Param("hfleFile") HfleFile hfleFile);
}
