package com.tianhao.luo.service.impl;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.UserFileRecode;
import com.tianhao.luo.domain.mapper.UserFileRecodeMapper;
import com.tianhao.luo.service.UserFileAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * @ClassName: UserFileAnalysisServiceImpl
 * @Description: 用户文件分析，包括一天内最高下载前十；一周最高下载量前十；一个月最高下载量前十
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  10:50
 */
@Service("UserFileAnalysisServiceImpl")
public class UserFileAnalysisServiceImpl implements UserFileAnalysisService {

    private static final Integer ONE = 1;

    @Autowired
    private UserFileRecodeMapper userFileRecodeMapper;

    @Override
    public List<HfleFile> listTopTenFromCurrentDay() {
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.of(0, 0, 0));
        List<UserFileRecode> userFileRecodes = userFileRecodeMapper.listTopTenUserFileRecode(localDateTime.minusDays(ONE), localDateTime);
        return userFileRecodeMapper.listTopTenHfleFile(userFileRecodes);
    }

    @Override
    public List<HfleFile> listTopTenFromCurrentWeek() {
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.of(0, 0, 0));
        List<UserFileRecode> userFileRecodes = userFileRecodeMapper.listTopTenUserFileRecode(localDateTime.minusWeeks(ONE), localDateTime);
        return userFileRecodeMapper.listTopTenHfleFile(userFileRecodes);
    }

    @Override
    public List<HfleFile> listTopTenFromCurrentMonth() {
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.of(localDate, LocalTime.of(0, 0, 0));
        List<UserFileRecode> userFileRecodes = userFileRecodeMapper.listTopTenUserFileRecode(localDateTime.minusMonths(ONE), localDateTime);
        return userFileRecodeMapper.listTopTenHfleFile(userFileRecodes);
    }
}
