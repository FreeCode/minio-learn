package com.tianhao.luo.service.impl;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.domain.mapper.HfleFileMapper;
import com.tianhao.luo.domain.mapper.UserFileRecodeMapper;
import com.tianhao.luo.domain.repository.HfleFileRepository;
import com.tianhao.luo.service.UserFileService;
import com.tianhao.luo.uitls.FileType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @ClassName: UserFileServiceImpl
 * @Description: 用户文件服务实现
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/9  20:33
 */
@Service("UserFileServiceImpl")
public class UserFileServiceImpl implements UserFileService {
    @Autowired
    private HfleFileRepository hfleFileRepository;
    @Autowired
    private HfleFileMapper hfleFileMapper;
    @Autowired
    private UserFileRecodeMapper userFileRecodeMapper;

    @Override
    public Page<HfleFile> queryUsersFile(HiamUser hiamUser, PageRequest pageRequest) {
        HfleFile hfleFile = new HfleFile();
        hfleFile.setTenantId(hiamUser.getTenantId());
        return hfleFileRepository.findAll(Example.of(hfleFile), pageRequest);
    }

    @Override
    public Page<HfleFile> queryUsersFileByType(HiamUser hiamUser, String type, PageRequest pageRequest) {
        HfleFile hfleFile = new HfleFile();
        hfleFile.setTenantId(hiamUser.getTenantId());
        for (FileType fileType :
                FileType.values()) {
            if (fileType.getType().equals(type)) {
                hfleFile.setFileType(fileType.getMinioType());
            }
        }
        return hfleFileRepository.findAll(Example.of(hfleFile), pageRequest);
    }

    @Override
    public HfleFile queryFileByUserAndUuid(HiamUser hiamUser, String uuid) {
        HfleFile hfleFile = new HfleFile();
        hfleFile.setTenantId(hiamUser.getTenantId());
        hfleFile.setAttachmentUuid(uuid);
        return hfleFileMapper.selectLimitOne(hfleFile);
    }

    @Override
    public void insertRecode(HiamUser hiamUser, String uuid, Date date) {
        userFileRecodeMapper.insertRecode(hiamUser, uuid, date);
    }
}
