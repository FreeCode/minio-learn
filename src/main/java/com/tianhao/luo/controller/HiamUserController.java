package com.tianhao.luo.controller;

import com.tianhao.luo.domain.entity.HiamUser;
import com.tianhao.luo.service.HiamUserService;
import com.tianhao.luo.uitls.CommonException;
import com.tianhao.luo.uitls.ResponseData;
import com.tianhao.luo.uitls.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HiamUserController
 * @Description: 用户服务接口，能够创建新的用户，和更新用户信息
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:20
 */
@RestController
@RequestMapping("/user")
public class HiamUserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HiamUserController.class);

    @Autowired
    private HiamUserService hiamUserService;

    /**
     * 插入一个用户
     * @param hiamUser 用户基本信息
     * @return 用户基本信息
     */
    @PostMapping
    public ResponseData<HiamUser> saveUser(HiamUser hiamUser) {
        try {
            return Result.success(hiamUserService.createUser(hiamUser));
        } catch (CommonException e) {
            LOGGER.error(e.toString());
            return Result.fail(hiamUser);
        }
    }
}
