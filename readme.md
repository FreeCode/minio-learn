[toc]

## 目的

能实现文件的上传下载，且能对多个文件上传进行多线程执行，提高程序对上传的响应。
能够对用户们下载的文件进行分析，列出榜单

## 技术

基于springboot实现，若以后某些springcloud项目需要一个文件服务模块，则可以将该模块引入。
只需引入eureka的依赖，将其挂在eureka注册中心上，通过网关来进行调用即可。

能够了解到什么？

1. JPA的使用，用来进行对表的查询；能实现分页操作；公共字段的抽离
2. MyBatis动态sql，及遍历查询。@Param注解的使用（不使用）对应在xml中的对象属性的使用
3. Minio的上传和下载sdk的使用，io流中字节流（缓冲）的使用
4. 建造者模式的实例使用
5. 配置选项的抽离，将配置项抽离到application.yml文件中
6. 泛型的使用。不限于返回结果的使用、工具类中的使用
7. aop中切面的定义，及对注解进行分析
8. 多线程上传文件，使用线程池执行上传任务
9. 枚举类的定义及使用

## 环境

docker-compose搭建minio、redis、nginx和mysql容器

- minio：作为对象存储，存放用户的文件，并且提供了可视化后端管理。能提供基本类型的预览，比如图片、pdf、MP4格式的视频。
- nginx： 帮助minio实现文件代理。用户访问文件不走minio的端口。
- mysql：持久化用户上传的文件数据，实现急速秒传

## 表和库准备

库准备：

```mysql
create schema minio_learn;
```

表准备：

```mysql
CREATE TABLE `hfle_file` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attachment_uuid` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0' COMMENT '附件集UUID',
  `directory` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '上传目录',
  `file_url` varchar(480) COLLATE utf8mb4_bin NOT NULL COMMENT '文件地址',
  `file_type` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件类型',
  `file_name` varchar(60) COLLATE utf8mb4_bin NOT NULL COMMENT '文件名称',
  `file_size` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `bucket_name` varchar(60) COLLATE utf8mb4_bin NOT NULL COMMENT '文件目录',
  `md5` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件MD5',
  `tenant_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '租户ID',
  `object_version_number` bigint(20)  DEFAULT '1' COMMENT '行版本号，用来处理锁',
  `creation_date` datetime  DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20)  DEFAULT '-1',
  `last_updated_by` bigint(20)  DEFAULT '-1',
  `last_update_date` datetime  DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`),
  KEY `hfle_file_n1` (`file_url`,`bucket_name`,`tenant_id`),
  KEY `hfle_file_n2` (`attachment_uuid`,`tenant_id`),
  KEY `md5_index` (`md5`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
```

```mysql
CREATE TABLE `hiam_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '租户ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户姓名',
  `user_email` varchar(50) NOT NULL COMMENT '用户邮箱',
  `object_version_number` bigint(20) NOT NULL DEFAULT '1' COMMENT '行版本号',
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL DEFAULT '-1',
  `last_updated_by` bigint(20) NOT NULL DEFAULT '-1',
  `last_update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
```
