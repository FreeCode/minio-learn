package com.tianhao.luo.service;

import com.tianhao.luo.domain.entity.HfleFile;

import java.util.List;

/**
 * @ClassName: UserFileAnalysisService
 * @Description: 用户文件分析，包括一天内最高下载前十；一周最高下载量前十；一个月最高下载量前十
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/10  10:45
 */
public interface UserFileAnalysisService {
    /**
     * 一天最高下载榜单
     * @return 文件记录
     */
    List<HfleFile> listTopTenFromCurrentDay();
    /**
     * 一周最高下载榜单
     * @return 文件记录
     */
    List<HfleFile> listTopTenFromCurrentWeek();
    /**
     * 一月最高下载榜单
     * @return 文件记录
     */
    List<HfleFile> listTopTenFromCurrentMonth();
}
