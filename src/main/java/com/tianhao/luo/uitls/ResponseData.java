package com.tianhao.luo.uitls;

/**
 * @ClassName: Result
 * @Description: 响应信息格式化
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  17:21
 */
public class ResponseData<T> {

    private final T data;
    private String msg;
    private Integer code;

    public ResponseData(T data, String msg, Integer code) {
        this.data = data;
        this.msg = msg;
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
