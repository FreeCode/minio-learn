package com.tianhao.luo.service;

import com.tianhao.luo.domain.entity.HfleFile;
import com.tianhao.luo.domain.entity.HiamUser;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName: HfleFileService
 * @Description: 文件服务
 * @author: tianhao.luo@hand-china.com
 * @date: 2020/8/6  22:09
 */
public interface HfleFileService {
    /**
     * 上传文件
     * @param hiamUser 用户基本信息
     * @param file 文件
     * @return 数据库中文件的基本信息
     */
    HfleFile save(HiamUser hiamUser, MultipartFile file);

    /**
     * 浏览器文件下载
     * @param hfleFile 文件对象
     * @param httpServletResponse 响应
     */
    void downLoadFile(HfleFile hfleFile, HttpServletResponse httpServletResponse);

    /**
     * 上传文件
     * @param hiamUser 用户基本信息
     * @param files 文件
     * @return 数据库中文件的基本信息
     * @throws Exception 业务异常
     */
    List<HfleFile> batchSaveFile(HiamUser hiamUser, List<MultipartFile> files) throws Exception;
}
